var app = angular.module("index", []);

var height;
var width;
var padding;
var clicado=false;

function sizeOfThings() {
	var h=document.getElementById('gra').clientHeight;
	var w=document.getElementById('gra').clientWidth;	
	height = (300*h)/502;
	width = (600*w)/669;
	padding = 30; 
}

sizeOfThings();

app.run(function($rootScope) {
	window.addEventListener('resize', function(){
		sizeOfThings();
		if(clicado==true){
			$rootScope.$$childTail.gerarGrafico();
		}
	}); 
});

app.controller("ProdutosController", ['$scope','$http','$filter',function($scope,$http,$filter) {

	$http.get('_js/produtos2.json').then(function(response){

            $scope.produtos = response.data.produtos;
            $scope.listaDeProdutos = $scope.produtos;

    });

	function verificaProdutoNoAno(name,ano) {
		var flag = 0;
		if($scope.produtos) {
			$scope.produtos.forEach( function(element, index) {				
				if(element.nome.localeCompare(name)===0 && element.data == ano){					
					flag=1;
				}
			});
		}
		if(flag==1){
			return true;
		}else{
			return false;
		}
	}

	$scope.adicionarProduto = function(){
		var dataF = new Date();

		if(verificaProdutoNoAno($scope.produto.nome,dataF.getFullYear())===false){
			$scope.produtos.push({
				nome: $scope.produto.nome,
				valor: $scope.produto.valor,
				quantidade: $scope.produto.quantidade,
				data: dataF.getFullYear()
			});	
		}else{
			$scope.produtos.forEach( function(element, index) {
				if (element.nome.localeCompare($scope.produto.nome)===0 && element.data == dataF.getFullYear()) {
					element.quantidade += $scope.produto.quantidade;
				}
			});
		}	

		$scope.produto.nome = $scope.produto.valor = $scope.produto.quantidade = "";

		// var res = $http.post('_js/produtos_json',$scope.produtos);
		// res.success(function(data, status, headers, config) {
		// 		alert("Deu!");
		// 	});
		// res.error(function(data, status, headers, config) {
		// 	alert( "failure message: " + JSON.stringify({data: data}));
		// });
	};

	var orderBy = $filter('orderBy');

	$scope.ordenar = function(predicate) {
	    $scope.predicate = predicate;
	    $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
	    $scope.produtos = orderBy($scope.produtos, predicate, $scope.reverse);
	};

	$scope.ordenar('valor');	

	/*D3*/
	$scope.gerarGrafico = function(){		

		d3.select("div.grafico").selectAll("svg").remove();
		d3.select("div.grafico").selectAll("div").remove();
		var canvas = d3.select("div.grafico");

		//tratamento dos dados
		var dataArray = [];
		var dataTimeArray = [];
		var dArray = [];
		var cont=0;
		var dat;

		function verificadorDeData(year,vetor) {
			var flag = 0;
			if(vetor){				
				vetor.forEach( function(element, index) {
					if(element.ano == year){						
						flag = 1;
					}
				});
			}
			if(flag==1){
				return true;
			}else{
				return false;  
			}
		}

		angular.forEach($scope.produtos,function(value,index){
			if(verificadorDeData(value.data,dataArray)===false){					
	        	dataArray.push({
	        		"ano":value.data,
	        		"quantidade":value.quantidade
	        	});
	        }else{
	        	dataArray.forEach( function(element, index) {
	        		if(element.ano == value.data){
	        			element.quantidade += value.quantidade;
	        		}
	        	});
	        }
	    });
	    //----------------------------------------------------
	    function compare(a,b) {
		  if (a.ano < b.ano)
		     return -1;
		  if (a.ano > b.ano)
		    return 1;
		  return 0;
		}

	    dataArray = dataArray.sort(compare);
		
		canvas = canvas.append("svg")
							.attr("id","tempGraph")
							.attr("height",height)
							.attr("width",width)
							.style("margin","0 1.1% 0 0.5%");

		var xScale = d3.scale.linear()								
								.domain(d3.extent(dataArray, function(d) { return d.ano; }))
								.range([padding, width - padding * 2]);															

		var yScale = d3.scale.linear()
								.domain([0, d3.max(dataArray, function(d) { return d.quantidade; })])
								.range([height - padding, padding]);													

		var xAxis = d3.svg.axis()
								.scale(xScale)							
								.orient("bottom")						
								.ticks(dataArray.length-1,".");								

		var yAxis = d3.svg.axis()
								.scale(yScale)							
								.orient("left")
								.ticks(dataArray.length);	

		var plotArea = canvas.append('g')
		    .attr('clip-path', 'url(#plotAreaClip)');

		plotArea.append('clipPath')
		    .attr('id', 'plotAreaClip')
		    .append('rect')
		    .attr({ width: width, height: height });														
																				
		canvas.append("g")						
				.call(xAxis)
				.attr("class","x axis")
				.attr("transform", "translate(0," + (height-padding) + ")")	
				.append("text")
				.attr("x",540)
				.attr("y",-6)
				.style("text-anchor","end")
				.text("Ano");			

		canvas.append("g")			
				.call(yAxis)
				.attr("class","y axis")
				.attr("transform", "translate(" + padding + ",0)")
				.append("text")
				.attr("transform", "rotate(90)")
				.attr("y",-18)
				.attr("x",30)
				.attr("dy", ".92em")
				.text("Quantidade");	

		var lineGen = d3.svg.line()
								.x(function(d) {									
									return xScale(d.ano);
								})										
								.y(function(d) {
									return yScale(d.quantidade);
								});
								//.interpolate("basis");

		canvas.append("path")
					.attr('d', lineGen(dataArray))
					.attr('stroke', 'green')
					.attr('stroke-width', 2)
					.attr('fill', 'none')
					.attr('id','pathone');		

		dataArray.forEach( function(element, index) {
			canvas.append("rect")
				.attr("x",xScale(element.ano))
				.attr("y",yScale(element.quantidade))
				.attr("height",15)
				.attr("width",70)
				.attr("stroke","gray")
				.attr("stroke-width","2")
				.attr("class","label")
				.attr("id","rect"+index)
				.style({opacity:'0.0'})
				.on('mouseover', function(d){
				    var nodeSelection = d3.select(this).style({opacity:'1.0'});
				   	canvas.select("text#label"+index).style({opacity:'1.0'});
				})
				.on('mouseout', function(d){
				    var nodeSelection = d3.select(this).style({opacity:'0.0'});
				   	canvas.select("text#label"+index).style({opacity:'0.0'});
				})
				.attr("fill","green");
			canvas.append("text")
				.attr("x",xScale(element.ano)+5)
				.attr("y",yScale(element.quantidade)+10)
				.attr("class","label")
				.attr("id","label"+index)
				.attr("fill","#fff")
				.style("font","0.7em Arial")
				.style({opacity:'0.0'})
				.on('mouseover', function(d){	
					canvas.select("text#label"+index).style({opacity:'1.0'});			    
				   	canvas.select("#rect"+index).style({opacity:'1.0'});
				})
				.on('mouseout', function(d){				    
				   	canvas.select("#rect"+index).style({opacity:'0.0'});
				   	canvas.select("text#label"+index).style({opacity:'0.0'});
				})				
				.text("Quant.: "+element.quantidade);
		});									

		function getInterpolation() {

			var interpolate = d3.scale.linear()
										.domain([0, 1])
										.range([1, dataArray.length + 1]);

			return function(t) {

				var flooredX = Math.floor(interpolate(t));
				var interpolatedLine = dataArray.slice(0, flooredX);
				  
				if(flooredX > 0 && flooredX < dataArray.length) {
				  var weight = interpolate(t) - flooredX;
				  var weightedLineAverage = dataArray[flooredX].y * weight + dataArray[flooredX-1].y * (1-weight);
				  interpolatedLine.push( {"x":interpolate(t)-1, "y":weightedLineAverage} );
				  }

				return lineGen(interpolatedLine);						
				  		
			};
		}

		d3.select("#pathone")
			.transition()
			.duration(1000)	
			.attrTween("d", getInterpolation);	

		d3.select("div.grafico")
					.append("div")											
					.style("margin-top",10)					
					.text("Todos os Produtos")	
						.append("p")
						.append("svg")
						.attr("height",10)
						.attr("width",10)
						.style("margin","0 1.1% 0 0.5%")
							.append("circle")
							.attr("cx",5)
							.attr("cy",5)
							.attr("r",5)
							.attr("fill","green");				

		//Gerando Grafico De Navegacao
		var navWidth = width,
		    navHeight = 100 - padding;

		var navChart = d3.select('div.grafico').classed('chart', true).append('svg')
		    .classed('navigator', true)
		    .attr('width', navWidth + padding)
		    .attr('height', navHeight + padding + padding)
		    .append('g')
		    .attr('transform', 'translate(' + padding + ',' + padding + ')');

		var navXScale = d3.scale.linear()								
			.domain(d3.extent(dataArray, function(d) { return d.ano; }))
			.range([padding, navWidth - padding * 2]);															

		var navYScale = d3.scale.linear()
			.domain([0, d3.max(dataArray, function(d) { return d.quantidade; })])
			.range([navHeight - padding, padding]);	

		var navXAxis = d3.svg.axis()
		    .scale(navXScale)
		    .orient('bottom')
		    .ticks(dataArray.ano,".");

		navChart.append('g')
		    .attr('class', 'x axis')
		    .attr('transform', 'translate(0,' + navHeight + ')')
		    .call(navXAxis);

		var navData = d3.svg.area()
		    .x(function (d) { return navXScale(d.ano); })
		    .y0(navHeight)
		    .y1(function (d) { return navYScale(d.quantidade); });

		var navLine = d3.svg.line()
		    .x(function (d) { return navXScale(d.ano); })
		    .y(function (d) { return navYScale(d.quantidade); });

		navChart.append('path')
		    .attr('class', 'data')
		    .attr('d', navData(dataArray));

		navChart.append('path')
		    .attr('class', 'line')
		    .attr('d', navLine(dataArray));

		var viewport = d3.svg.brush()
		    .x(navXScale)
		    .on("brush", function () {
		        xScale.domain(viewport.empty() ? navXScale.domain() : viewport.extent());
		        redrawChart();
		    });

		function redrawChart() {

			canvas.select("g.x.axis")
				.call(xAxis);			

			canvas.selectAll("path#pathone").remove();
		    canvas.append("path")
					.attr('d', lineGen(dataArray))
					.attr('stroke', 'green')
					.attr('stroke-width', 2)
					.attr('fill', 'none')
					.attr('id','pathone');	

			canvas.selectAll("text.label").remove();
			canvas.selectAll("rect.label").remove();
			dataArray.forEach( function(element, index) {
				canvas.append("rect")
					.attr("x",xScale(element.ano))
					.attr("y",yScale(element.quantidade))
					.attr("height",15)
					.attr("width",70)
					.attr("stroke","gray")
					.attr("stroke-width","2")
					.attr("class","label")
					.attr("id","rect"+index)
					.style({opacity:'0.0'})
					.on('mouseover', function(d){
					    var nodeSelection = d3.select(this).style({opacity:'1.0'});
					   	canvas.select("text#label"+index).style({opacity:'1.0'});
					})
					.on('mouseout', function(d){
					    var nodeSelection = d3.select(this).style({opacity:'0.0'});
					   	canvas.select("text#label"+index).style({opacity:'0.0'});
					})
					.attr("fill","green");
				canvas.append("text")
					.attr("x",xScale(element.ano)+5)
					.attr("y",yScale(element.quantidade)+10)
					.attr("class","label")
					.attr("id","label"+index)
					.attr("fill","#fff")
					.style("font","0.7em Arial")
					.style({opacity:'0.0'})
					.on('mouseover', function(d){	
						canvas.select("text#label"+index).style({opacity:'1.0'});			    
					   	canvas.select("#rect"+index).style({opacity:'1.0'});
					})
					.on('mouseout', function(d){				    
					   	canvas.select("#rect"+index).style({opacity:'0.0'});
					   	canvas.select("text#label"+index).style({opacity:'0.0'});
					})				
					.text("Quant.: "+element.quantidade);
			});						
		}

		navChart.append("g")
		    .attr("class", "viewport")
		    .call(viewport)
		    .selectAll("rect")
		    .attr("height", navHeight);

		var minDate = d3.min(dataArray, function(d) { return d.ano; });	
		var maxDate = d3.max(dataArray, function(d) { return d.ano; });		    
        		    
		var zoom = d3.behavior.zoom()
		    .x(xScale)
		    .on('zoom', function() {
		    	var x;		    	
		        if (xScale.domain()[0] < minDate) {
			   		x = zoom.translate()[0] - xScale(minDate) + xScale.range()[0];
		            zoom.translate([x, 0]);
		        } else if (xScale.domain()[1] > maxDate) {
			    	x = zoom.translate()[0] - xScale(maxDate) + xScale.range()[1];
		            zoom.translate([x, 0]);
		        }
		        redrawChart();
		        updateViewportFromChart();
		    });

		function updateViewportFromChart() {

		    if ((xScale.domain()[0] <= minDate) && (xScale.domain()[1] >= maxDate)) {

		        viewport.clear();
		    }
		    else {

		        viewport.extent(xScale.domain());
		    }

		    navChart.select('.viewport').call(viewport);
		}

		var overlay = d3.svg.area()
		    .x(function (d) { return xScale(d.ano); })
		    .y0(0)
		    .y1(height);

		plotArea.append('path')
		    .attr('class', 'overlay')
		    .attr('d', overlay(dataArray))
		    .call(zoom);

		viewport.on("brushend", function () {
		        updateZoomFromChart();
		    });

		function updateZoomFromChart() {

		    zoom.x(xScale);
		    
		    var fullDomain = maxDate - minDate,
		        currentDomain = xScale.domain()[1] - xScale.domain()[0];

		    var minScale = currentDomain / fullDomain,
		        maxScale = minScale * 20;

		    zoom.scaleExtent([minScale, maxScale]);
		}       		

	};

	$scope.gerarGraficoPorProduto = function(){
		d3.select("div.grafico").selectAll("svg").remove();
		d3.select("div.grafico").selectAll("div").remove();
		var canvas = d3.select("div.grafico");

		//tratamento dos dados
		var dataArray = [];
		var dataTimeArray = [];
		var dArray = [];
		var cont=0;
		var flag;
		var dat;

		function verificadorDeProdutos(nome,vetor) {
			flag = false;			
			vetor.forEach( function(element, index) {								 
				if(element.produto.localeCompare(nome)===0){
					flag = true;				 						
				}
			});	 
			if(flag){
				return 1;
			}else{
				return 0;
			}
		}

		function verificadorDeAnos(year,vetor) {
			flag = false; cont = 0;					
			vetor.forEach( function(element, index) {								 
				if(element.ano==year){
					flag = true;	
					cont = element.quantidade+1;			 						
				}
			});									 					
			if(flag){
				return cont;
			}else{
				return 0;
			}
		}		
		
		angular.forEach($scope.produtos,function(value,index){					
	        if(verificadorDeProdutos(value.nome,dataArray)===0){	       		        	
		        dataArray.push({
		        	"produto":value.nome,
		        	"dados":[{
			        	"ano":value.data,
			        	"quantidade":value.quantidade
			        }]
		        });
		    }else{		    	    
		    	dataArray.forEach( function(element, index) {
		    		if(element.produto.localeCompare(value.nome)===0){
		    			if(verificadorDeAnos(value.data,element.dados)===0){
			    			element.dados.push({
			    				"ano":value.data,
			        			"quantidade":value.quantidade
			    			});
			    		}else{			    			
			    			element.dados.forEach( function(element2, index) {
			    				if(element2.ano == value.ano){
			    					element2.quantidade += value.quantidade;
			    				}
			    			});	
			    		}	
		    		}
		    	});
		    }    
	    });		

	    var ar=[];
	    for (var ed= 0; ed < dataArray.length; ed++) {
	    	ar.push(d3.max(dataArray[ed].dados,function (d) { return d.quantidade; }));
	    }

	    function compare(a,b) {
		  if (a.ano < b.ano)
		     return -1;
		  if (a.ano > b.ano)
		    return 1;
		  return 0;
		}

		dataArray.forEach( function(element, index) {			
			element.dados = element.dados.sort(compare);
		});

		canvas = canvas.append("svg")
							.attr("id","tempGraph")
							.attr("height",height)
							.attr("width",width)
							.style("margin","0 1.1% 0 0.5%");							

		var xScale = d3.scale.linear()								
								.domain(d3.extent($scope.produtos, function(d) { return d.data; }))
								.range([padding, width - padding * 2]);															

		var yScale = d3.scale.linear()
								.domain([0, d3.max(ar)])
								.range([height - padding, padding]);													

		var xAxis = d3.svg.axis()
								.scale(xScale)							
								.orient("bottom")						
								.ticks(dataArray.length-1,".");

		var yAxis = d3.svg.axis()
								.scale(yScale)							
								.orient("left")
								.ticks(dataArray.length);								
																				
		canvas.append("g")						
				.call(xAxis)
				.attr("class","axis")
				.attr("transform", "translate(0," + (height-padding) + ")")	
				.append("text")
				.attr("x",540)
				.attr("y",-6)
				.style("text-anchor","end")
				.text("Ano");			

		canvas.append("g")			
				.call(yAxis)
				.attr("class","axis")
				.attr("transform", "translate(" + padding + ",0)")
				.append("text")
				.attr("transform", "rotate(90)")
				.attr("y",-18)
				.attr("x",30)
				.attr("dy", ".92em")
				.text("Quantidade");

		var lineGen = d3.svg.line()
								.x(function(d) {
									return xScale(d.ano);
								})
								.y(function(d) {
									return yScale(d.quantidade);
								});
								//.interpolate("basis");

		var color = d3.scale.category20();

		for (var i = 0; i < dataArray.length; i++) {	
			var cor = color(i);		
			canvas.append("path")
						.attr('d', lineGen(dataArray[i].dados))
						.attr('stroke', cor)
						.attr('stroke-width', 2)
						.attr('fill', 'none')
						.attr('id','pathone');

			d3.select("div.grafico")
						.append("div")											
						.style("width","50px")
						.style("margin","0 0 0 5%")
						.style("float","left")
						.text(dataArray[i].produto)	
							.append("p")
							.append("svg")
							.attr("height",10)
							.attr("width",10)
							.style("margin","0 1.1% 0 3.5%")
								.append("circle")
								.attr("cx",5)
								.attr("cy",5)
								.attr("r",5)
								.attr("fill",cor);


		}			

	};

}]);

function showMe (place) {
	document.getElementById(place).style.display='table';  
	sizeOfThings();
}

function clicandoNoClicador(){
	clicado = true;
}